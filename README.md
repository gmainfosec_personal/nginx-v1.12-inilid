# Nginx-1.12.0 (Inilid) Web Service for Mubasher|DirectFN #

This is how we compiled Nginx with new security and performance features. Please refer nginx.spec file for compiled options. **We have compiled it for RHEL/CentOS v7 only.**

Here build Nginx with ChaCha20 and Poly1305 protocols on OpenSSL for better performances for Mobile devices.
```
$ cd rpmbuild/SOURCE
$ wget https://www.openssl.org/source/openssl-1.1.0f.tar.gz
$ tar -zxvf openssl-1.0.2h.tar.gz
```

Now let's build the Nginx package
```
$ cd SPECS
$ rpmbuild -bb nginx.spec
...
warning: Could not canonicalize hostname: euca-172-16-10-239.eucalyptus.internal
Wrote: /home/hirantha/rpmbuild/RPMS/x86_64/nginx-1.10.2-1.el7.centos.ngx.x86_64.rpm
Wrote: /home/hirantha/rpmbuild/RPMS/x86_64/nginx-module-xslt-1.10.2-1.el7.centos.ngx.x86_64.rpm
Wrote: /home/hirantha/rpmbuild/RPMS/x86_64/nginx-module-image-filter-1.10.2-1.el7.centos.ngx.x86_64.rpm
Wrote: /home/hirantha/rpmbuild/RPMS/x86_64/nginx-module-geoip-1.10.2-1.el7.centos.ngx.x86_64.rpm
Wrote: /home/hirantha/rpmbuild/RPMS/x86_64/nginx-module-perl-1.10.2-1.el7.centos.ngx.x86_64.rpm
Wrote: /home/hirantha/rpmbuild/RPMS/x86_64/nginx-debuginfo-1.10.2-1.el7.centos.ngx.x86_64.rpm
```

**Forward Secrecy & Diffie Hellman Ephemeral Parameters**

The concept of forward secrecy is simple: client and server negotiate a key that never hits the wire, and is destroyed at the end of the session. The RSA private from the server is used to sign a Diffie-Hellman key exchange between the client and the server. The pre-master key obtained from the Diffie-Hellman handshake is then used for encryption. Since the pre-master key is specific to a connection between a client and a server, and used only for a limited amount of time, it is called Ephemeral.

With Forward Secrecy, if an attacker gets a hold of the server's private key, it will not be able to decrypt past communications. The private key is only used to sign the DH handshake, which does not reveal the pre-master key. Diffie-Hellman ensures that the pre-master keys never leave the client and the server, and cannot be intercepted by a MITM.

All versions of nginx as of 1.4.4 rely on OpenSSL for input parameters to Diffie-Hellman (DH). Unfortunately, this means that Ephemeral Diffie-Hellman (DHE) will use OpenSSL's defaults, which include a 1024-bit key for the key-exchange. Since we're using a 2048-bit certificate, DHE clients will use a weaker key-exchange than non-ephemeral DH clients.

We need generate a stronger DHE parameter:
```
cd /etc/nginx/ssl
openssl dhparam -out dhparam.pem 4096
```
And then tell nginx to use it for DHE key-exchange:
```
ssl_dhparam /etc/nginx/ssl/dhparam.pem;
```

# Implement HTTP Strict Transport Security (HSTS)

HTTPS (HTTP encrypted with SSL or TLS) is an essential part of the measures to secure traffic to a website, making it very difficult for an attacker to intercept, modify, or fake traffic between a user and the website.

When a user enters a web domain manually (providing the domain name without the **http://** or **https://** prefix) or follows a plain **http://** link, the first request to the website is sent unencrypted, using plain HTTP. Most secured websites immediately send back a redirect to upgrade the user to an HTTPS connection, but a well-placed attacker can mount a man-in-the-middle (MITM) attack to intercept the initial HTTP request and can control the user�s session from then on.

Setting the Strict Transport Security (STS) response header in NGINX and NGINX Plus is relatively straightforward:
```
add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
```

The **always** parameter ensures that the header is set for all responses, including internally-generated error responses.

# Enable OCSP Stapling (for Godaddy) and combine and create the trusted certificate

* Download root and intermediate cert files from Godaddy
```
$ wget https://certs.godaddy.com/repository/gdroot-g2.crt
$ wget https://certs.godaddy.com/repository/gdig2.crt
$ cat gdig2.crt > godaddy.crt
$ cat gdroot-g2.crt >> godaddy.crt
```

* Add following entires to the nginx.conf file at http {} section
```
..
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.4.4 8.8.8.8 valid=300s;
ssl_trusted_certificate /etc/nginx/ssl/godaddy.crt;
..
```

* Restart the nginx and check OCSP stapling is enabled
```
systemstcl restart nginx 
```
```
$ openssl s_client -connect 127.0.0.1:443 -status
CONNECTED(00000003)
depth=3 C = US, O = "The Go Daddy Group, Inc.", OU = Go Daddy Class 2 Certification Authority
verify return:1
depth=2 C = US, ST = Arizona, L = Scottsdale, O = "GoDaddy.com, Inc.", CN = Go Daddy Root Certificate Authority - G2
verify return:1
depth=1 C = US, ST = Arizona, L = Scottsdale, O = "GoDaddy.com, Inc.", OU = http://certs.godaddy.com/repository/, CN = Go Daddy Secure Certificate Authority - G2
verify return:1
depth=0 OU = Domain Control Validated, CN = *.mubashertrade.com
verify return:1
OCSP response:
======================================
OCSP Response Data:
    OCSP Response Status: successful (0x0)
    Response Type: Basic OCSP Response
    Version: 1 (0x0)
    Responder Id: C = US, ST = Arizona, L = Scottsdale, O = GoDaddy Inc., CN = Go Daddy Validation Authority - G2
    Produced At: Nov 24 11:34:32 2016 GMT
    Responses:
    Certificate ID:
...
```

Noticed the OCSP Response Status: successful; which means OSCP enabled

Enjoy!